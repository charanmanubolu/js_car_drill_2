/*// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory.
 Execute a function to find what the make and model of the last car in the inventory is? 
  Log the make and model into the console in the format of:
"Last car is a *car make goes here* *car model goes here*"

 */

function problem2(arr) {
    try {
        let lastValue=arr.reduce((cv,acc )=> {
            return acc
        },{})
             console.log(`last car is a ${lastValue.car_make} and ${lastValue.car_model}`)

    } catch (error) {
        console.log("An error occurred", error)
    }

}
module.exports = problem2