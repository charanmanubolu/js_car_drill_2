/*

// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  
Execute a function and return an array that only contains BMW and Audi cars. 
 Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console. 
 */


function problem6(arr) {
    let result = []
    try {
        arr.filter(Element => {
            if (Element.car_make === "Audi" || Element.car_make === "BMW") {
                result.push(Element)
            }
        })
    } catch (error) {
        console.error("An error occurred", error);
    }
    return JSON.stringify(result);

}

module.exports = problem6