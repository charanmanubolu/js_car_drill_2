/*
// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website.
 Execute a function to Sort all the car model names into alphabetical order and log 
 the results in the console as it was returned.
 */

function problem3(arr) {
    let result = []
    try {
        arr.map(Element => {
            result.push(Element.car_model)
        })
    } catch (error) {
        console.log("An Error occurred", error)
    }
    return result.sort()
}
module.exports = problem3